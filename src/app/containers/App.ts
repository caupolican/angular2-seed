import {Component} from '@angular/core';

@Component({
  selector: 'fountain-app',
  template: require('./App.pug')
})
export class AppComponent {}
